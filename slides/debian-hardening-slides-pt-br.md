% Segurizando seu sistema:
% Caio Volpato (caioau) \
  [caioau.keybase.pub](https://caioau.keybase.pub/) → [caioauheyuuiavlc.onion](http://caioauheyuuiavlc.onion/) \
    210B C5A4 14FD 9274 6B6A  250E **EFF5 B2E1 80F2 94CE** \
    Todos os copylefts são lindos: Licenciado sob [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
% [Criptofesta -- 7 Dez 2019](https://cryptorave.org/) 

---
title: 'Segurizando seu sistema:'
subtitle: 'Como deixar seu Debian super seguro'
date: Cryptorave 2019
author:
- Caio Volpato (caioau) [caioau.keybase.pub](https://caioau.keybase.pub/)
linkcolor: blue
urlcolor: blue
theme:
- Darmstadt
colortheme:
- rose
...

## Tópicos:

* Importância de ser software livre para segurança.
* Considerações iniciais.
* Mantendo seu sistema atualizado.
* Mitigando spectre e meltdown (microcode).
* Mitigando ataques badUSB com usbguard.
* Enjaulando programas com firejail.
* Habilitando aleatorização de MAC adresses.
* Habilitando firewall (ufw).
* Hardenizando o linux kernel.
* Outros sistemas operacionais seguros.

<!---

Esse slide eh gerado usando pandoc com beamer, para gerar o pdf rode:

pandoc -t beamer input.md -o output.pdf

se vc não tem o pandoc instale-o com:

sudo apt install pandoc texlive-latex-recommended

leia o manual do pandoc

https://pandoc.org/MANUAL.html#producing-slide-shows-with-pandoc


https://github.com/lfit/itpol/blob/master/linux-workstation-security.md

-->

--- 

### Casa hacker hackerspace

A Casa Hacker é um espaço hacker sem fins lucrativos e 100% dedicada a colocar comunidades locais no controle de suas experiências digitais e a moldarem o futuro da tecnologia da informação e comunicação para o bem público. Nós colocamos nossos princípios acima dos lucros e acreditamos que as tecnologias da nossa era são recursos públicos a serem explorados e construídos por todos, não uma mercadoria a ser vendida. Em um coletivo experiente, multidisciplinar e líder em tecnologia e sociedade desenvolvemos iniciativas de impacto social que empodera pessoas e transforma comunidades.

* Localizado em Campinas (campo grande).
* Website: [casahacker.org](https://casahacker.org)
* Redes sociais: @casahacker
* Mastodon: [\@casahacker@masto.donte.com.br](https://masto.donte.com.br/@casahacker)


---


### Importância do software livre para a segurança: 


Você (infelizmente) já deve ter ouvido que software livre é inerentemente inseguro pois qualquer pessoa consegue ver o código e com isso atacar o software.

Essa estrategia esconder como um sistema funciona para protegê-lo, conhecida como segurança por obscuridade é rejeitada pelos especialistas há seculos (desde 1851).

* Princípio de Kerckhoffs: um sistema criptográfico deve ser seguro mesmo que tudo sobre o sistema, exceto a chave, seja de conhecimento público. 

---

### Importância do software livre para a segurança: 

Entretanto, um software não vai se tornar magicamente seguro somente por ser software livre, o projeto precisa proativamente trabalhar para ter uma boa segurança.

Nessa palestra [No IT security without Free Software](https://invidio.us/watch?v=B0qxm331Q8Q) o autor elenca vantagens de segurança de softwares livres, como:

* Transparência para todos: além de auditorias de segurança independentes aumenta a confiança de todos.
* Pressão externa: Como o código será publico isso aumenta a pressão para tomar mais cuidado.
* Compartilhando sinergias: Outros usuários e a comunidade tomam interesse e contribuem.
* Independência: Não precisa esperar o fabricante para consertar o código ou ate mesmo fazer um fork.


---

## Considerações iniciais: 

Antes de colocar as medidas propostas em ação, precisamos nós certificar que algumas coisas estão corretas:

### Segurança física:

**Nunca deixe seu computador sozinho**, mesmo se desligado ou bloqueado. Existem ataques como os Evil maid que contornam a criptografia de disco.

Alternativamente, para mitigar esses ataques instale o debian com o /boot num "pendrive" e o proteja-o nunca deixando plugado quando seu computador estiver sozinho.


---

### Intel Management engine (ME):

O Intel Management engine é um OS (baseado em minix) que roda na sua CPU, foi criado com o intuito de gerenciar remotamente os computadores mesmo desligados (introduzido em 2008). 

Ele tem acesso super privilegiado ao seu sistema (ring -3 (3:userspace,0:kernelspace,-1:hypervisor)), e não pode ser desativado por software.

Além de ser proprietário, ele é um super risco de segurança uma vez dado o que ele pode acessar/fazer.

São conhecidas vulnerabilidades super graves como: CVE-2017-5689 e CVE-2017-5705.

Para "neutralizar" o intel me, temos duas opções: se seu computador é suportado por coreboot/libreboot, caso contrario usar o [me_cleaner](https://github.com/corna/me_cleaner). **Isso requer que você desmonte completamente seu computador e altere sua BIOS.**

Se você tem AMD, não está imune uma vez que a AMD tem uma tecnologia semelhante.


---


### Disco criptografado com senha segura:

Quando instalando o Debian selecione para criptografar todo o disco, e coloque uma senha dadoware (6+ palavras sorteadas no dado)

* A partir do Debian 10 (buster) o cryptsetup suporta luks2 que utiliza argon2 para derivar as chaves, muito mais seguro que o padrão antigo.

### Swap também criptografado:

Além do disco todo criptografado o swap também deve ser criptografado, se você selecionou o particionamento automático isso já será feito (luks+lvm). 

* Alternativamente, eu tenho utilizado o [zram](https://apt.galliumos.org/pool/main/z/zram-config/zram-config_0.5-galliumos2_all.deb), o swap em RAM comprimido, ele tem um desempenho melhor.

---

### Faça backups:

Faça sempre backup, seu HD pode falhar, seu computador pode ser roubado/perdido. Lembrando que o **backup deve ser em outra mídia que não seu HD, deve ser automático e periódico.** 

Existem várias opções:

* Android com syncthing.
* raspi+HD externo com cron+rsync.
* nuvem com nextcloud com cryptomator.
* nuvem com tarsnap. 

---

### Use gerenciador de senhas:

Use um gerenciador de senhas para ter senhas únicas e seguras para todas suas contas, na senha mestre dele use uma senha dadoware (6+ palavras sorteadas no dado: [livreto](https://github.com/thoughtworks/dadoware)).

A opção mais recomendada é o KeePassXC e KeePass DX no android (e sincronizar com syncthing).

Caso sincronizar entre Android e PC seja um problema pra você, alternativas:

* LessPass que não armazena nada (deriva a senha desejada a partir da senha mestra mais domínio)
* Bitwarden que armazena na nuvem (tem opção de auto hospedar). 

---


### Use Autenticação em 2 fatores (2FA)

A senha sozinha não é suficiente para te proteger! Ative autenticação em 2 fatores em suas contas: em [turnon2fa](https://www.telesign.com/turnon2fa/) da instruções para cada serviço. 

Não esqueça de ativar nos mensageiros como Signal, ~~WhatsApp, Telegram~~

* App para Android: use o andOTP: da fdroid.

* Chaves de segurança (yubikey): Dependendo do seu modelo de ameaças, é indicado usar uma chave de segurança como yubikey, veja [dongleauth.info](https://www.dongleauth.info/) se um serviço critico para você suporta essa autenticação (U2F). 


Yubikeys são muito caras e são proprietárias uma alternativa acessível é o [tomu](http://tomu.im/) por ~60R$.


---

### Firefox hardening:

É super indicado hardenizar seu Firefox, ou melhor ainda usar Tor Browser o máximo possível. 

No Firefox instale algumas extensões indicadas nessa pagina e configure [algumas configs](https://www.privacytools.io/browsers/#about_config).


---

### Mantendo seu sistema atualizado

Manter seu sistema atualizado é essencial para se manter seguro.

Para computadores pessoais recomendo usar um programa que te notifica quando você precisa atualizar.

> sudo apt install pk-update-icon

E configure o comando atualizador:

> package-update-indicator-prefs

e coloque **gpk-update-viewer** como comando atualizador e coloque para atualizar **a cada hora**.

* Outra dica é assinar a lista [Debian Security announcements](https://lists.debian.org/debian-security-announce/)

---

### Mitigando Spectre e Meltdown 

Spectre e Meltdown são vulnerabilidades que exploram como os processadores foram projetados.

Para mitigar vamos instalar o microcode da intel ou amd, que **é proprietário**.

* Você precisa ter o **non-free** habilitado no seu /etc/apt/sources.list 

```
sudo apt install intel-microcode # processadores intel 
sudo apt install amd64-microcode # processadores amd
```

* Checando (depois de um reboot): 

```
curl -L https://meltdown.ovh -o spectre-meltdown-checker.sh
chmod +x spectre-meltdown-checker.sh
sudo ./spectre-meltdown-checker.sh
```

---

### Mitigando ataques badUSB com usbguard

Ataques badUSB ou rubber ducky são aqueles onde um dispositivo malicioso é conectado na USB e então ele se comporta como um teclado e instala algo malicioso. 

Vamos usar o usbguard, toda vez que você conectar algo na USB ele vai exibir um popup perguntando se quer autorizar (ou não) aquele dispositivo.

> sudo apt install usbguard usbguard-applet-qt

* Desconecte todas as portas USB e gere as regras padrão:

```
sudo usbguard generate-policy > rules.conf
sudo install -m 0600 -o root -g root rules.conf /etc/usbguard/rules.conf
```

* Se seu teclado e mouse forem USB, não os desconectem senão ele não serão autorizados.

* Finalizando: Edite o `/etc/usbguard/usbguard-daemon.conf` e adicione o seu usuario em IPCAllowedUsers e IPCAllowedGroup, finalmente habilite e inicie o daemon: sudo systemctl enable usbguard

---

### Enjaulando programas com firejail 

Firejail previne que programas possivelmente vulneráveis comprometa seu sistema, ele enjaula seus programas e limita o que cada programa pode fazer. 

Para instalar: 

> sudo apt install firejail firejail-profiles firetools

Para versões mais recentes, use o backports:

> sudo apt -t buster-backports firejail firejail-profiles firetools


---

### Firejail: configuração:

* Crie a pasta de configuração:

> mkdir -p ~/.config/firejail/

* Crie as regras globais:

```
# ~/.config/firejail/globals.local

net none
apparmor
```

Isso irá desativar rede (e internet) para todos os programas, exceto os que a gente explicitamente vamos permitir, e habilitar apparmor.

---

### Firejail: configuração:

* Permitindo rede para alguns programas, como navegadores:

```
mkdir ~/browsersHome/
# ~/.config/firejail/firefox.local

ignore net
private ~/browsersHome/

```

Dessa forma, o navegador terá uma /home própria, isolando seus arquivos da sua home verdadeira.

---

### Firejail: configuração:

* Permitindo U2F nos navegadores: no arquivo `/etc/firejail/firejail.config` procure e des-comente `browser-disable-u2f no`

* Habilitando firejail para todos programas:

```
sudo firecfg
sudo firecfg --fix-sound
```

Dessa forma, todos os programas serão enjaulados.

* Dica: firetools: Existem um interface gráfica para monitorar os programas enjaulados e propriedades.

---

### Habilitando aleatorização de MAC address

Isso é mais relacionado a privacidade, mesmo assim vamos fazer: O endereço físico do seu WiFi pode ser usado pra rastrear por quais rede você conecta, logo por onde anda.

O network manager (aquele ícone de conectar na barra de status) tem essa funcionalidade, tornando bem super fácil habilitar a randomização.

```
# /etc/NetworkManager/conf.d/30-mac-randomization.conf

[device-mac-randomization]
wifi.scan-rand-mac-address=yes

[connection-mac-randomization]
ethernet.cloned-mac-address=random
wifi.cloned-mac-address=random
```

Se a rede requer logar naquelas paginas (captive portal) faz sentido manter um MAC aleatório fixo pra aquela rede, veja a [explicação](https://blogs.gnome.org/thaller/2016/08/26/mac-address-spoofing-in-networkmanager-1-4-0/)

---

### Habilitando um firewall (ufw)

Vamos habilitar um firewall para filtrar (pelo menos) pacotes de entrada:

```
sudo apt install ufw
sudo ufw default deny incoming 
sudo ufw enable
```

---

### Hardenizando o Kernel Linux

* Configurações sysctl: São configurações usadas em tempo de execução do kernel, foram retiradas do tails


---

```
# /etc/sysctl.d/tails.conf

net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 0
fs.protected_fifos = 2
fs.protected_regular = 2
kernel.kexec_load_disabled = 1
kernel.kptr_restrict=2
vm.mmap_rnd_bits=32
vm.mmap_rnd_compat_bits=16
net.ipv4.tcp_mtu_probing=1
kernel.yama.ptrace_scope=2
net.ipv4.tcp_timestamps=0
kernel.unprivileged_bpf_disabled=1

# alguns navegadores como Brave precisam disso, descomente
#kernel.unprivileged_userns_clone=1
```

---

### Hardenizando o Kernel Linux

* Parâmetros de boot: De novo copiado do: [tails -- kernel hardening](https://tails.boum.org/contribute/design/kernel_hardening/)

```
# /etc/default/grub
# adicione ao final de GRUB_CMDLINE_LINUX_DEFAULT=

slab_nomerge slub_debug=FZP mce=0 page_poison=1 pti=on \
mds=full,nosmt module.sig_enforce=1 vsyscall=none 
```


---

## Outros sistemas operacionais seguros:

Nessa palestra falei de ações e mitigações no Debian, mas existem sistemas operacionais que foram projetados com segurança em primeiro lugar.

### Tails:

É um sistema operacional baseado em Debian, só pode ser instalado em mídias moveis como pendrives, SD cards e CDs. Não deixa nenhum rastro no computador e todas conexões de redes são roteadas através da rede Tor. 

---

### QubesOS:

É um sistema operacional que utiliza uma abordagem diferente para segurança: segurança por compartimentação, utilizando maquinas virtuais para limitar o dado caso o sistema seja comprometido. 

* Infelizmente o QubesOS não é suportado por muitos computadores, pois ele precisa que a CPU seja capaz dessas virtualizações.

* No qubesOS o whonix pode ser instalado, um OS que separa o tor (programa) do Tor browser (em VMs diferentes).

---

### Arquitetura do QubesOS

![](qubes-arch-diagram-1.png)

---

### OpenBSD

É um sistema operacional da família BSD com enfase em segurança, com várias mitigações de segurança.

